%This script is for Problem 1a for Math 375 HW 5

%%%%%%%%%%%%%%%% PART 1 A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% b = zeros(1,1122);
% for i=0:560
%    b(1,((2*i) + 1)) = xnod(i+1,1);
% end
% 
% for i=1:561
%     b(1,(2*i)) = ynod(i,1);
% end
% 
% for i=1:1122
%     b(1,i) = 0;
% end
% b(1,1111) = 1/5;
% 
% %Computing delta x for use in computing the new positions of the nodes
%     dx = A\b';
% %computing loads in x and y directions
%  xload = xnod + dx(1:2:end); 
%  yload = ynod + dx(2:2:end);
% 
% trussplot(xnod,ynod,bars)
% hold on
% trussplot(xload,yload,bars)
% hold on
%plot(b(411),3,'r*')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%% PART 1 B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% totaltime = 0;
% for i = 0:100;
%     t = cputime;
%     dx = A\b'
%     e = (cputime - t);
%     totaltime = (totaltime + e);
% end
% averagetime = (totaltime/100)

%%%%%%%%%%% AVERAGE TIMES %%%%%%%%%%%%%%
%           N = 261: 0.0132s           % 
%           N = 399: 0.0497s           % 
%           N = 561: 0.1251s           % 
%           N = 1592: 2.3410s          % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% n1 = 261; t1 = 0.0132;
% n2 = 399; t2 = 0.0497;
% n3 = 561; t3 = 0.1252;
% n4 = 1592; t4 = 2.3410;
% n = [261,399,561,1592];
% t = [0.0132,0.0497,0.1252,2.3410];
% 
% loglog(n,t,'k',n1,t1,'y*',n2,t2,'y*',n3,t3,'y*',n4,t4,'y*','LineWidth',5)
% set(gca,'FontSize',18)
% xlabel('N')
% ylabel('Time (seconds)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART 1 C %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tic
% b = zeros(1,1122);
% for i=0:560
%    b(1,((2*i) + 1)) = xnod(i+1,1);
% end
% 
% for i=1:561
%     b(1,(2*i)) = ynod(i,1);
% end
% 
% dxcollection = zeros(1122,1);
% %computing dx for each individual node with the rest of them set to 0
% for j=1:1122
%     for i=1:1122
%         b(1,i) = 0;
%     end
%     b(1,j) = 1/5;
% 
%     dx = A\b';
%     
%     %array of the norms of each displacement to see which is largest.
%     dxcollection(j,1) = norm(dx);
% 
% end
% 
% %loop to see who the weakest node is
% largestdx = dxcollection(1,1);
% weakestnode = 1;
% for k = 1:1122
%     if (largestdx <= dxcollection(k,1))
%         largestdx = dxcollection(k,1);
%         weakestnode = k;
%     end
% end
% toc

% set all elements of b to 0
% for i=1:522
%     b(1,i) = 0;
% end
% b(1,weakestnode) = 1/5;
% 
% dx = A\b';
%     
% xload = xnod + dx(1:2:end); 
% yload = ynod + dx(2:2:end);
% 
% trussplot(xnod,ynod,bars)
% hold on
% trussplot(xload,yload,bars)
% hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A1: WEAKEST NODE IS NODE 515  %
% A2: WEAKEST NODE IS NODE 789  %
% A3: WEAKEST NODE IS NODE 1111 % 
% A4: WEAKEST NODE IS NODE 3165 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% ELAPSED TIMES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        A1 = Elapsed time is 3.553536 seconds.       % 
%        A2 = Elapsed time is 19.302457 seconds.      % 
%        A3 = Elapsed time is 71.727409 seconds.      % 
%        A4 = Stopped computation after 5 minutes.    % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % I ESTIMATE THAT OPTIMIZING THE CODE WILL CUT THE TIME BY AT MOST 10s

%%%%%%%%%%% BELOW IS MY OPTIMIZED CODE %%%%%%%%%% 
% tic
% b = zeros(1,3184);
% for i=0:1591
%    b(1,((2*i) + 1)) = xnod(i+1,1);
% end
% 
% for i=1:1592
%     b(1,(2*i)) = ynod(i,1);
% end
% 
% dxcollection = zeros(3184,1);
% %computing dx for each individual node with the rest of them set to 0
% for j=1:3184
%     for i=1:3184
%         b(1,i) = 0;
%     end
%     b(1,j) = 1/5;
% 
%     [L, U, p] = lu(A, 'vector');
%     z = linsolve(L, (b(p))', struct('LT', true));
%     x = linsolve(U, z,    struct('UT', true));
%     
%     %array of the norms of each displacement to see which is largest.
%     dxcollection(j,1) = norm(x);
% 
% end
% 
% %loop to see who the weakest node is
% largestdx = dxcollection(1,1);
% weakestnode = 1;
% for k = 1:3184
%     if (largestdx <= dxcollection(k,1))
%         largestdx = dxcollection(k,1);
%         weakestnode = k;
%     end
% end
% toc

%%%%%%%%%% OPTIMIZED ELAPSED TIMES %%%%%%%%%%%%%%%%%
%   A1 = Elapsed time is 3.382691 seconds.         %
%   A2 = Elapsed time is 16.205328 seconds.        %
%   A3 = Elapsed time is 63.048029 seconds.        %
%   A4 = Computation stopped after 5 mins.         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART 1 D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A = sparse(A);
tic
b = zeros(1,3184);
for i=0:1591
   b(1,((2*i) + 1)) = xnod(i+1,1);
end

for i=1:1592
    b(1,(2*i)) = ynod(i,1);
end

dxcollection = zeros(3184,1);
%computing dx for each individual node with the rest of them set to 0
for j=1:3184
    for i=1:3184
        b(1,i) = 0;
    end
    b(1,j) = 1/5;

    dx = A\b';
    
    %array of the norms of each displacement to see which is largest.
    dxcollection(j,1) = norm(dx);

end

%loop to see who the weakest node is
largestdx = dxcollection(1,1);
weakestnode = 1;
for k = 1:3184
    if (largestdx <= dxcollection(k,1))
        largestdx = dxcollection(k,1);
        weakestnode = k;
    end
end
toc

% %%%%%%%%%%% BELOW IS MY OPTIMIZED CODE %%%%%%%%%% 
A = sparse(A);
tic
b = zeros(1,3184);
for i=0:1591
   b(1,((2*i) + 1)) = xnod(i+1,1);
end

for i=1:1592
    b(1,(2*i)) = ynod(i,1);
end

dxcollection = zeros(3184,1);
%computing dx for each individual node with the rest of them set to 0
for j=1:3184
    for i=1:3184
        b(1,i) = 0;
    end
    b(1,j) = 1/5;

    [L, U] = lu(A);
    z = L \ b';
    x = U \ z;
    
    %array of the norms of each displacement to see which is largest.
    dxcollection(j,1) = norm(x);

end

%loop to see who the weakest node is
largestdx = dxcollection(1,1);
weakestnode = 1;
for k = 1:3184
    if (largestdx <= dxcollection(k,1))
        largestdx = dxcollection(k,1);
        weakestnode = k;
    end
end
toc

%%%%%%%%%%%%%%%%%%% SPARSE TIMES %%%%%%%%%%%%%%%%%%
% WITHOUT LU                                      %  
% A1: Elapsed time is 1.045888 seconds.           %
% A2: Elapsed time is 2.615975 seconds.           %  
% A3: Elapsed time is 5.466972 seconds.           %
% A4: Elapsed time is 47.309079 seconds.          %
%                                                 %   
% WITH LU                                         %
% A1: Elapsed time is 0.680761 seconds.           %
% A2: Elapsed time is 1.776469 seconds.           %
% A3: Elapsed time is 4.374796 seconds.           %
% A4: Elapsed time is 72.445644 seconds.          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%% TIME SAVED %%%%%%%%%%%%
% WITHOUT LU                       %  
% A1: 2.5076s                      %
% A2: 16.6865s                     %  
% A3: 66.2604s                     %
% A4: more than 4 min              %
%                                  %   
% WITH LU                          %
% A1: 2.7019                       %
% A2: 14.4289                      %
% A3: 58.6732                      %
% A4: more than 4 min              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
