#include <stdio.h> 

int PrimalityTest(int n)
{

  int i, j, mod, prime; 

  for (i = 2; i <= n; i++)
    {
      prime = 1;

      for (j = (i - 1); j > 1; j--) //decriments divisor j by 1 to check all numbers < i s.t. j>1 for j%i==0
      {
        mod = (i % j);
        if ((mod) == 0 )
          {
             prime = 0;
             break;
          }
      } 
        if (prime == 1) //if prime == 1, then the number is prime and gets printed, else begin another "i" for loop iteration
          printf ("%d IS PRIME!\n", i);    
    }

}

main () 
{

  int number;

  printf ("ENTER A NUMBER AND I'LL TELL YOU ALL THE PRIME NUMBERS THAT ARE LESS THAN OR EQUAL TO IT.\n\n");
  scanf ("%d", &number);

  PrimalityTest( number );
   
}

