/*
This is a program that provides info on 
gifs in question that are of version 87a, 89a
*/

#include <stdio.h>

int getGif( char *array )
{
  char gifele; //single element of a gif header
  int index = 0;
  int count = 0;

  gifele = getchar();
  while ( gifele != EOF ) 
  { 
    ++count;
    array[index] = gifele;
    ++index;
    gifele = getchar();
  }
  
  if ( count != 13 )
  {
    printf ("BAD GIF FILE! (There are too few or too many characters in the header)");
    return 0;
  }  
  else
    return *array; 
}

void ColorStuff( int hex, int bci )
{

  int color;
  int sort;
  int entries;
  int ls; //left shift  
  int entryno; //number of entries 

  color = ( hex & 0x80 );
  
  if ( color == 0x80 );  
  {
    sort = ( hex & 0x08 );
    entries = ( hex & 0x07 );
    ls = ( ( hex & 0x07 ) + 0x01 );    
    entryno = ( 0x01 << ls ); 

    printf ("\nThis gif has a color table.\n");
    
    if (sort == 0x08)  
      printf ("\nThis color table IS sorted.\n");
    else printf ("\nThe color table IS NOT sorted\n");   
   
    printf ("\nNumber of entries: %d\n", entryno);
    
    printf ("\nThe background color index: %d\n\n", bci); 
    
  }


}


int main (void)
{

  int index = 0;
  char gifarray[13];
  int header = 0;

  char GIF[6] = {0,0,0,0,0,0};
  char width[2] = {0,0}; 
  char height[2] = {0,0};
  int pack; //packed fields
  
  int w1; //width
  int h1; //height 

  header = getGif(gifarray);
  
  for ( index = 0; index < 6; index++ ) //Assignment of gif and 87a/89a
  {
    if ( gifarray[index] == '\0' )
      break;
    else GIF[index] = gifarray[index]; 
  }

  width[0] = gifarray[7];
  width[1] = gifarray[6];
  w1 = (( width[0] << 8 ) | ( width[1] & 0xff ) );

  height[0] = gifarray[9];
  height[1] = gifarray[8];
  h1 = ((height[0] << 8 ) | ( height[1] & 0xff ) );  

  pack = gifarray[10];

  if ( GIF == "gif89a" || "GIF89a"|| "gif87a" || "GIF87a" )
  {
    printf ("\nWidth in pixels: %i\n", w1 );
    printf ("\nHeight in pixels: %i\n", h1 ); 
    ColorStuff( pack, gifarray[11] );
    return; 
  } 

  else printf ("The gif version is invalid.");

}

