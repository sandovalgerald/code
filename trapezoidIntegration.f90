!
! This program is for integrating
!
! I = Int(e^(cos(k*x)))dx from -1 to 1
!
! For the valus of k = pi, and k = (pi)^2
!
! Xi = Xl + Itr*H, where Itr is from 0 to N.
!
program trapezoid

    implicit none
    double precision :: temp,H,a,b,Xi,Xn,answer
    integer :: Nmax,Itr,n
    
    Nmax = 1000
    a = -1d0
    b = 1d0
   
    do n = 2, 5000 !Nmax - 1 
        H = ((b-a)/dble(n))
        answer = ((0.5d0)*(f(a) + f(b)))
  
        do Itr = 1, n - 1
            Xn = (a + dble(Itr)*H)
            answer = (answer + f(Xn))
        end do
        answer = H*answer
        print *,answer
    end do
    !print *, 'The result for k = Pi is:',answer
    !print *, 'The result for k = Pi^2 is: ',answer     

contains

    function f(x)
    real(kind = 8), intent(in) :: x
    real(kind = 8) :: f
    real(kind = 8), parameter :: Pi = acos(-1d0)
    !f = (EXP(cos(Pi*x)))
    f = (EXP(cos(Pi*Pi*x)))
    end function f

end program trapezoid

   
  
